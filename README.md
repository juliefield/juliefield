# Hi! My name is Julie Field



## Designer & Frontend Developer

My name is Julie Field and I'm a designer and Frontend developer in Columbus, Ohio. I pride myself on not just building solutions that clients love, but also producing results when you need it. To me, there's few things better than landing on a beautifully crafted website or engaging with an application that provides the ability to pleasantly navigate and find information quickly and easily. I believe that beauty should not just stop at the visual. The same attention to detail that goes into my designs, I place into my code. I pride myself on writing code that is clean and fast. I also inspire to create solutions that my clients can maintain today and into the future.

## Let's Connect

[My Website](https://www.juliefield.design/)

[LinkedIn](https://www.linkedin.com/in/juliemfield/)

[Dribbble](https://dribbble.com/juliefield)


Want to add your own User profile README? [Learn More](https://docs.gitlab.com/ee/user/profile/#user-profile-readme)

